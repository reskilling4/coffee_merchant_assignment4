/*
	TIME DIMENSION CREATION
*/

-- Get date range for time dimension boundaries
SELECT MIN("OrderDate"), MAX("OrderDate") 
FROM orders;
--> 2005-10-01 to 2006-04-18.


DROP TABLE IF EXISTS dim_time;
CREATE TABLE dim_time (
	"Date" DATE PRIMARY KEY,
	"Year" INT,
	"Month" INT,
	"Day" INT,
	"DayOfWeek" VARCHAR(9)
);

-- Here we generate the time dimension table, also including the day of week ('Monday' to 'Sunday') as a separate column.
-- We write hard-coded dates for simplicity, but we could also have used an extra join to orders instead.
INSERT INTO dim_time (
	SELECT date, EXTRACT (year FROM date) AS year, EXTRACT (month FROM date) AS month, EXTRACT (day FROM date) AS day, TRIM(TO_CHAR(date, 'Day')) AS dayofweek
	FROM (SELECT generate_series('2005-10-01'::timestamp, '2006-04-18'::timestamp, '1 day'::interval) as date) 
	ORDER BY date ASC
);


SELECT * FROM dim_time;
-- Looks good, let's export it as CSV
