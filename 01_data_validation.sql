/*
	BELOW ARE THE VALIDATION STEPS OF THE ETL PROCESS
*/

/*
	DATA VALIDATION - Consumer
*/
-- Check for NULLs
SELECT * FROM consumers
WHERE "FirstName" is NULL OR
"LastName" is NULL OR
"Street" is NULL OR
"City" is NULL OR
"State" is NULL OR
"Zipcode" is NULL OR
"Phone" is NULL OR
"Fax" is NULL;
--> None!

-- Check for duplicated customers sharing same FirstName, LastName and Phone 
SELECT "FirstName", "LastName", "Phone" FROM consumers
GROUP BY "FirstName", "LastName", "Phone"
HAVING COUNT(*) > 1;
--> None, i.e no pure duplicates.

-- Check for duplicated customers sharing same Phone number
SELECT * FROM consumers
WHERE "Phone" IN (
SELECT "Phone" FROM consumers
GROUP BY "Phone"
HAVING COUNT(*) > 1)
ORDER BY "Phone";
--> 4 duplicates, but none of them seem to share the same Name, so we will not aggregate them

--> data in consumers table is sufficiently valid.

/*
	DATA VALIDATION - Inventory
*/
-- Check for NULLs
SELECT * FROM inventory
WHERE "Name" is NULL OR
"Price" is NULL OR
"ItemType" is NULL;
--> None

-- Check for duplicate Names
SELECT "Name", COUNT(*) FROM inventory
GROUP BY "Name"
HAVING COUNT(*) > 1;
--> None

-- Check for negative Price, negative OnHand
SELECT * FROM inventory
WHERE "Price" < 0 OR
"OnHand" < 0;
--> 8 items with negative OnHand. This does not affect the sales fact though so we leave it as is

--> data in inventory table is sufficiently valid.

/*
	DATA VALIDATION - orderlines
*/
-- Check for NULLs
SELECT * FROM orderlines
WHERE "InventoryID" is NULL OR
"Quantity" is NULL OR
"Price" is NULL OR
"Discount" is NULL;
--> None

-- Check for negative Quantity, Price or Discount
SELECT * FROM orderlines
WHERE "Quantity" < 0 OR
"Price" < 0 OR
"Discount" < 0;
--> None

--> data in orderlines table is sufficiently valid

/*
	DATA VALIDATION - orders
*/
-- Check for NULLs
SELECT * FROM orders
WHERE "OrderDate" is NULL OR
"ConsumerID" is NULL;
--> None

-- Check OrderDate range to use correct range in our time dimension.
SELECT MIN("OrderDate"), MAX("OrderDate") FROM orders;
--> 2005-10-01 to 2006-04-18

--> Data in orders is sufficiently valid.
