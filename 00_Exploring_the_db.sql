-- In this file we perform some exploration of the db. This is not part of the ETL pipeline.


-- Check if there are inconsistencies in Prices between inventory and orders. Could be normal, prices could be evolving over time.
SELECT COUNT(*) as InconsistentPrices
FROM (SELECT i."Price" as invPrice, ol."Price" as olPrice, (i."Price"-ol."Price") as diff
FROM inventory i
JOIN orderlines ol ON i."InventoryID" = ol."InventoryID")
WHERE diff != 0;

-- Dig deeper in price inconsistencies. Check which item and sort by highest difference.
-- This shows the evolution of the inventory price compared to the day the item was ordered.
select o."OrderDate", ol."InventoryID", ol."Price" as olPrice, i."Price" as invPrice, (i."Price"-ol."Price") as diff
from orderlines ol
JOIN orders o ON o."OrderID" = ol."OrderID"
JOIN inventory i ON i."InventoryID" = ol."InventoryID"
ORDER BY o."OrderDate";

select * from orderlines;
select * from inventory
order by "Price" DESC;


-- Check the discount range to understand how to apply it
SELECT MIN("Discount"), MAX("Discount") FROM orderlines;
--> Discount is between 0 and 0.25. It is most probably a percentage value (0% to 25% discount).

-- !! Important choice here, how we treat the discount column. We make the assumption that the discount 
-- is NOT included in the price, therefore when calculating total sales we calculate the "actual_price" as:
-- actual_price = Price * (1 - Discount).