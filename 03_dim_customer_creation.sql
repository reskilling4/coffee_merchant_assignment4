/*  
	CUSTOMER DIMENSION
*/

/* dim_customer (CustomerID, Name, ContactDetails):

We will only use columns from the consumers table.
Following are the column mappings we will perform from consumers -> dim_customer:
- ConsumerID -> CustomerID
- Concatenate FirstName, LastName -> Name

For ContactDetails we have to denormalize the data. We will create a function that can merge all 
contact info into a single text field with following format:

	Address: <Street>, <City>, <State> <Zipcode>
	Phone: <Phone>
	Fax: <Fax>

Then we insert the data and export as dim_customer.csv
*/

DROP TABLE IF EXISTS dim_customer;

CREATE TABLE dim_customer (
	"CustomerID" INT PRIMARY KEY,
	"Name" VARCHAR(100) NOT NULL,
	"ContactDetails" TEXT NOT NULL
);

-- Here we define the contact details denormalization function that we will use later
CREATE OR REPLACE FUNCTION create_contact_details(street varchar, 
												  city varchar, 
												  state varchar,
												  zipcode varchar, 
												  phone varchar, 
												  fax varchar)
RETURNS text AS $$
BEGIN
	-- the E'\r\n' is the recommended way to insert line endings (windows style) in Postgres 
	RETURN  'Address: ' || street || ', ' || city || ', ' || state || ' ' || zipcode || E'\r\n' ||
			'Phone: ' || phone || E'\r\n' ||
			'Fax: ' || fax;		
END;
$$ LANGUAGE plpgsql;

-- Here we insert the data
INSERT INTO dim_customer
SELECT 
	"ConsumerID" as "CustomerID",
	("FirstName" || ' ' || "LastName")::VARCHAR(100) as "Name",
	create_contact_details("Street", "City", "State", "Zipcode", "Phone", "Fax")
FROM consumers;

SELECT * FROM dim_customer
-- Looks good, we export as dim_customer.csv