/*  
	FACT SALES
*/

/* fact_sales (sales_id, date, product_id, customer_id, quantity, total_sales):

Here we will join multiples tables to create our fact_sales table:
- inventory, consumers, orders, orderlines

Only total_sales will have to be computed. We will use the "Price" from the orderlines
table. The formula is as follow:

total_sales = (Quantity * Price) * (1 - Discount)

In the calculation Price is casted to decimal(5,2) type.


Then we export as fact_sales.csv
*/

DROP TABLE IF EXISTS fact_sales;

CREATE TABLE fact_sales (
	"SalesID" SERIAL PRIMARY KEY,
	"Date" DATE,
	"ProductID" INT,
	"CustomerID" INT,
	"Quantity" INT,
	"TotalSales" DECIMAL(5,2)
);

CREATE OR REPLACE FUNCTION total_sales(quantity INT, price REAL, discount REAL)
RETURNS DECIMAL AS $$
BEGIN
	-- We cast price and discount into a DECIMAL(5,2) prior calculation to avoid
	-- floating point calculation errors. 
	-- We explicitly cast the result into a DECIMAL(5,2).
	RETURN ((quantity * price::DECIMAL(5,2)) * (1 - discount::DECIMAL(5,2)))::DECIMAL(5,2);
END;
$$ LANGUAGE plpgsql;

SELECT * from orderlines;
-- Here we insert the data
INSERT INTO fact_sales("Date", "ProductID", "CustomerID", "Quantity", "TotalSales")
SELECT 
o."OrderDate", ol."InventoryID", c."ConsumerID", ol."Quantity", total_sales(ol."Quantity", ol."Price", ol."Discount") as total_sales
FROM orderlines ol
JOIN inventory i ON ol."InventoryID" = i."InventoryID"
JOIN orders o ON o."OrderID" = ol."OrderID"
JOIN consumers c ON c."ConsumerID" = o."ConsumerID"
ORDER BY "OrderDate" ASC;

SELECT * FROM fact_sales
-- Looks good, we export as fact_sales.csv