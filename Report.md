## Report, Coffee Merchant Assignment

### Authors
- Christian Asmar
- Branko Iljic
- Yoann Prigent

## Analysis

Here we explore the database and analyze it.
The datebase was created prior to this stage by executing the provided SQL 
instructions in `coffee_merchant.sql`

We then export the. ERD from pgAdmin as PNG file (see ERD.png) and look at it, see below:

![image](ERD.png)

### Findings
1) There are two "Price" columns: `inventory."Price"` and `orderlines."Price"`. 
We will have to decide which one to use in our sales fact table.
We can assume the following:
    - `orderlines."Price"` is what the customer has paid
    - `inventory."Price"` is the current listing price for the specific product.
Assuming the above, it could very well be that "Price" values are different over time for a specific product (InventoryID). For our sales fact table, if we are to include income from sales as a column, we should use `orderlines."Price"`.

2) There is an `orderlines."discount"` column, we'll have to figure out how to factor itin the sales.

3) Some tables will not be used at all for our datawarehouse star schema:
    - ``employees``
    - ``countries``
    - ``states``

4) Monetary values (price, discount) have type "Real" which is a floating point value. For accurate calculations we should change it to decimal.

5) `Customer` is called `consumer` in the original database.

6) There are no ContactDetails column for the customer, but many different attributes for it. We will denormalize those (i.e. merge) into a single column in our datawarehouse.

## ETL Process

### Step 1 - Validate the data 
Performed in ``01_data_validation.sql``

	Here we check for NULLs, negative values, duplicate entries in the tables of interest.


### Step 2 - Create Time Dimension
Performed in ``02_dim_time_creation.sql``

Signature:

**dim_time (date, year, month, day, dayofweek)**

- Generate series of dates larger than orders."OrderDate", i.e. starts earlier and ends later.
- Use the TO_CHAR(date, "Day") syntax to also include daynames as a column

--> Final export as ``dim_time.csv``


### Step 3 - Extract and Transform, Customer Dimension
Performed in ``03_dim_customer_creation.sql``

Signature:

**dim_customer (CustomerID, Name, ContactDetails)**

We will only use columns from the consumers table. Following are the column mappings we will perform from consumers -> dim_customer:
- ConsumerID -> CustomerID
- Concatenate FirstName, LastName -> Name

For ``ContactDetails`` we have to denormalize the data. We will create a function that can merge all contact info into a single text field with following format:

    Address: <Street>, <City>, <State> <Zipcode>
    Phone: <Phone>
    Fax: <Fax>

--> Final export as ``dim_customer.csv``


### Step 4 - Extract and Transform, Inventory Dimension
Performed in ``04_dim_inventory_creation.sql``. 

Signature:

**dim_inventory (ProductID, Name, Category, Price)**

We will only use columns from the ``inventory`` table.
Following are the column mappings we will perform from inventory -> dim_inventory:
- InventoryID -> ProductID
- Name -> Name
- ItemType -> Category (C -> Coffee, T -> Tea)
- Price -> Price (typecast into decimal)

--> Final export as ``dim_inventory.csv``


### Step 5 - Extract and Transform, Sales Fact table
Performed in ``05_fact_sales_creation.sql``

Signature:    
**fact_sales (sales_id, date, product_id, customer_id, quantity, total_sales)**

Here we will join multiples tables to create our fact_sales table:
- inventory
- consumers
- orders
- orderlines

``total_sales`` will have to be computed. We will use the "Price" from the orderlines table because it reflect what the consumer has paid. The formula is as follow:

    total_sales = (Quantity * Price) * (1 - Discount)

In the calculation ``Price`` and ``Discount`` are casted to decimal type to avoid floating point errors.

--> Final export as ``fact_sales.csv``


### Step 6 - Load the data into the datawarehouse
Performed in ``06_Load_data_into_datawarehouse.sql``

In pgAdmin we create a new database ``coffeemerchant_dw.db`` and we connect to it.

- create the 3 dimension tables and the fact table.
- import the respective CSV files using the COPY command. (Note: For this to work in pgAdmin we have to allow read-access to the CSV files to the postgres user)
-  Export the ER diagram into ``ERD_datawarehouse.png``:

![image](ERD_datawarehouse.png)


### Final Result

Below a screenshot of our fact sales in the datawarehouse:
![image](Final_result_fact_sales_in_DW.png)



## Reflections on the assignment


The most difficult part was what to do with the Contact Details from the customer, either to keep the many different attributres or to denormalize everything so that the dimension table is kept minimal. We chose the latter, but had to implement a somewhat complex SQL syntax with functions and a lot of concatenation,

Other difficulties were what to do with the duplicated `Price` columns, and how to use the column discount. We had to make assumptions, that the discount was not included in the price for example.

We also discussed how to implement the customer dimension and the second alternative is presented in the "branko" branch. The difference is that contact information like phone number, fax number etc. might not be interesting from a BI perspective and so the branch omits this information. However, the main branch includes this information with the argument that the user may want to filter on more detailed information. 



