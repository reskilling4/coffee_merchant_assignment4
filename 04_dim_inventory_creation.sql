/*  
	INVENTORY DIMENSION
*/

/* dim_inventory (ProductID, Name, Category, Price):

We will only use columns from the inventory table.
Following are the column mappings we will perform from inventory -> dim_inventory:
- InventoryID -> ProductID
- Name -> Name
- ItemType -> Category (C -> Coffee, T -> Tea)
- Price -> Price (cast to decimal type)

Then we export as dim_inventory.csv
*/

DROP TABLE IF EXISTS dim_inventory;

CREATE TABLE dim_inventory (
	"ProductID" INT PRIMARY KEY,
	"Name" VARCHAR(100) NOT NULL,
	"Category" VARCHAR(50) NOT NULL,
	"Price" DECIMAL(5,2) NOT NULL
);

-- Here we insert the data
INSERT INTO dim_inventory
SELECT 
	"InventoryID",
	"Name",
	CASE "ItemType" 
		WHEN 'C' THEN 'Coffee' 
		WHEN 'T' THEN 'Tea'
	END AS "Category",
	"Price"
FROM inventory;

SELECT * FROM dim_inventory
-- Looks good, we export as dim_inventory.csv