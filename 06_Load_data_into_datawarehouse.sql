/*  
	DATAWAREHOUSE LOAD DATA
*/

/* In this last step we will:

Create all dimension and fact tables
Load data from the previously exported CSV files

We are using a new empty database coffeemerchant_dw as the datawarehouse.
*/

DROP TABLE IF EXISTS dim_time, dim_customer, dim_inventory, fact_sales;
CREATE TABLE dim_time (
	"Date" DATE PRIMARY KEY,
	"Year" INT,
	"Month" INT,
	"Day" INT,
	"DayOfWeek" VARCHAR(9)
);

CREATE TABLE dim_customer (
	"CustomerID" INT PRIMARY KEY,
	"Name" VARCHAR(100) NOT NULL,
	"ContactDetails" TEXT NOT NULL
);

CREATE TABLE dim_inventory (
	"ProductID" INT PRIMARY KEY,
	"Name" VARCHAR(100) NOT NULL,
	"Category" VARCHAR(50) NOT NULL,
	"Price" DECIMAL(5,2) NOT NULL
);

CREATE TABLE fact_sales (
	"SalesID" SERIAL PRIMARY KEY,
	"Date" DATE REFERENCES dim_time("Date"),
	"ProductID" INT REFERENCES dim_inventory("ProductID"),
	"CustomerID" INT REFERENCES dim_customer("CustomerID"),
	"QuantitySold" INT NOT NULL,
	"TotalSales" DECIMAL(5,2) NOT NULL
);


-- Here we load the data into the warehouse. 
-- The CSV files permissions should be adjusted to that the postgres user 
-- (if running the code from pgAdmin) has read-access. 
-- 
COPY dim_time
FROM 'C:\Users\yprgcm\Documents\DE_Reskilling\C3_DB\Assignments\Assignment4\dim_time.csv' 
DELIMITER ',' 
CSV HEADER;

COPY dim_inventory
FROM 'C:\Users\yprgcm\Documents\DE_Reskilling\C3_DB\Assignments\Assignment4\dim_inventory.csv' 
DELIMITER ',' 
CSV HEADER;

COPY dim_customer
FROM 'C:\Users\yprgcm\Documents\DE_Reskilling\C3_DB\Assignments\Assignment4\dim_customer.csv' 
DELIMITER ',' 
CSV HEADER;

COPY fact_sales
FROM 'C:\Users\yprgcm\Documents\DE_Reskilling\C3_DB\Assignments\Assignment4\fact_sales.csv' 
DELIMITER ',' 
CSV HEADER;


SELECT * FROM fact_sales

-- Tadaa! Our Datawarehouse is complete.