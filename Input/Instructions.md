## Objectives
Extract relevant data from the provided SQL database.  
Transform this data into a format suitable for a data warehouse.  
Load the transformed data into a PostgreSQL database.

Design and implement the following tables:

`dim_time`: A dimension table to manage time-related data.  
`dim_customer`: A dimension table for storing customer data.  
`dim_inventory`: A dimension table for inventory items.  
`fact_sales`: A fact table that combines data from orders and order lines.

## Tasks

### Database Exploration

Examine the provided SQL file to understand the structure and relationships of the coffeemerchant database.  
Identify the tables and fields that will populate the dim_time, dim_customer, dim_inventory, and fact_sales tables in your data warehouse.  

### ETL Process

- Use an ETL tool to create ETL jobs that extract data from the source database.  
- Transform the data according to the requirements of the data warehouse schema.  
- Load the transformed data into the PostgreSQL database.

### Dimension and Fact Table Design

- Design the `dim_time` table to include fields such as date, month, year, and day of the week.
- Create the `dim_customer` table with customer-related information, such as customer ID, name, and contact details.
- Implement the `dim_inventory` table to include product-related data, like product ID, name, category, and price.
- Design the `fact_sales` table to record sales transactions, including keys to the dimension tables and metrics such as quantity sold and total sales.

### Data Loading and Validation

- Load the designed dimension and fact tables with the data transformed during the ETL process.
- Validate the data to ensure integrity and accuracy.

## Deliverables
Work in groups of 3-4. One person will submit the assignment and indicate who is in their group. 

- A report documenting the ETL process, including screenshots and descriptions of each step.
- The schema design for the dimension and fact tables, including SQL creation scripts.  
- A reflection on the challenges faced during the assignment and how they were overcome.  

This assignment requires you to apply the concepts of data warehousing, ETL processes, and database design. It will test your ability to extract, transform, and load data into a structured warehouse schema that supports analytical queries and reporting.